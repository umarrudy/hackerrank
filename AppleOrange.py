#(p1) Rumah Sam memiliki pohon apel dan pohon jeruk yang menghasilkan banyak buah. Dalam diagram di bawah ini, wilayah merah menunjukkan rumahnya, di mana s titik awal, dan t titik akhir. Pohon apel ada di sebelah kiri rumahnya, dan pohon jeruk ada di sebelah kanannya. Anda dapat mengasumsikan pohon-pohon tersebut terletak pada satu titik, di mana pohon apel berada di titik a, dan pohon jeruk berada di titik b.

#(p2) Ketika buah jatuh dari pohonnya, ia mendarat d unit jarak dari pohon asalnya di sepanjang x-axis. Nilai negatif d berarti buah jatuh d unit di sebelah kiri pohon, dan nilai positif berarti jatuh d unit ke kanan pohon.

#(p3) Diketahui nilai dari d untuk m apel dan n jeruk, tentukan berapa banyak apel dan jeruk yang jatuh di rumah Sam (mis., Dalam kisaran inklusif [s,t])?

#(p4) Misalnya, rumah Sam ada di antara s = 7 dan t = 10. Pohon apel terletak di a = 4 dan jeruk di b = 12. Ada m = 3 apel dan n = 3 jeruk. Pohon apple menjatuhkan apples = [2, 3, -4] jarak unit, dan oranges = [3, -2, -4] jarak unit. Dengan menambahkan setiap jarak apel ke posisi pohon, mereka mendarat di [4+2, 4+3, 4+-4] = [6, 7, 0]. Jeruk mendarat di [12+3 ,12+-2 ,12+-4] = [15, 10, 8]. Satu apel dan dua jeruk mendarat di kisaran inklusif 7 - 10 jadi kami cetak

# 1
# 2

#(Proses lebih gampang ketika digambar di kertas)

import functools

s = 7  # starting point sam's house
t = 11 # ending point sam's house location
a = 5  # location of the apple tree
b = 15  # location of the orange tree
apples = [2, 2, 1] # distance at which apple falls from the tree
oranges =  [5, -6] # distance at which oragne falls from the tree


def countApplesAndOranges(s, t, a, b, apples, oranges):
  treeToApple = list(map(lambda ap: ap + a , apples))
  treeToOrange = list(map(lambda org: org + b, oranges))
  apple = 0
  orange = 0
  for x in range(len(treeToApple)):
    if (treeToApple[x] >= s and treeToApple[x] <= t):
      apple += 1
  for y in range(len(treeToOrange)):
    if (treeToOrange[y] >= s and treeToOrange[y] <= t):
      orange +=1
  print(apple)
  print(orange)
  
countApplesAndOranges(s, t, a, b, apples, oranges)