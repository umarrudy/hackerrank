import functools
bill = [3, 10, 2, 9] 
k = 1 
n = 12

def bonApetit(bill, k, n):
  bill.remove(bill[k])
  su = functools.reduce(lambda x, y: x + y, bill)
  check = su / 2
  if(check == n):
    print("Bon Appetit")
  else:
    print(abs(check - n))
  
bonApetit(bill, k, n)
