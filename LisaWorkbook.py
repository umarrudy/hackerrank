n = 5 
k = 3 
arr = [4, 2, 6, 1, 10]

def workbook(n, k, arr):
  cnt = 0 # special problems
  i = 0   # chapter number
  j = 1   # page number
  m = 1   # problem number

  while i < n:
    # print(i)
    # print((m + k - 1, arr[i]))
    # print(min(m + k - 1, arr[i]))
    if m <= j and j <= min(m + k - 1, arr[i]): 
      cnt += 1
    j += 1
    m += k
    if m > arr[i]: # next chapter
      i += 1
      m = 1
  print(cnt)
workbook(n, k, arr)