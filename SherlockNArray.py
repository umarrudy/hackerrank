a = [1, 2, 3]
def balancedSums(a):
  summy = sum(a)
  l = len(a)
  lefty = 0
  for i in range(l):
    current = a[i]
    summy -= current
    if lefty == summy:
      print("Yes")
    lefty += current
  print("No")
balancedSums(a)
    
