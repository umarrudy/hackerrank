# Anda akan diberikan dua array integer dan diminta untuk menentukan semua integer yang memenuhi dua kondisi berikut:

# Elemen-elemen dari array pertama adalah semua faktor dari bilangan bulat yang dipertimbangkan
# 1. Bilangan bulat yang dipertimbangkan adalah faktor dari semua elemen array kedua
# 2. Angka-angka ini disebut sebagai antara dua array. Anda harus menentukan berapa angka yang ada.

# Misalnya, mengingat array a = [2,6] dan b = [24,36], ada dua angka di antara mereka: 6 dan 12. 6%2=0, 6%6=0, 24%6=0 dan 36%6=0 untuk nilai pertama. Demikian pula 12%2=0, 24%6=0 dan 24%12=0, 36%12=0.

# Selesaikan fungsi getTotalX di editor di bawah ini. Itu harus mengembalikan jumlah bilangan bulat yang antara set.

# getTotalX memiliki parameter berikut:

# a: array bilangan bulat
# b: array bilangan bulat




a = [2, 4]
b = [ 16, 32, 96 ]
def getTotalX(a, b):
  result = 0
  for i in range(1, 101):
      f = 0
      for j in a:
          if (i % j != 0):
              f = 1
              break
      if(f == 0):
          f2 = 0
          for k in b:
              if(k % i != 0):
                  f2 = 1
                  break
          if(f2 == 0):
              result += 1
  return result

s = getTotalX(a, b)
print(s)
